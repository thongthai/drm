/*
 * Copyright 2017 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
*/

#include <stdio.h>
#include <inttypes.h>

#include "CUnit/Basic.h"

#include "util_math.h"

#include "amdgpu_test.h"
#include "amdgpu_drm.h"
#include "amdgpu_internal.h"
#include "decode_messages.h"

#define IB_SIZE		4096
#define MAX_RESOURCES	16

struct amdgpu_vcn_bo {
	amdgpu_bo_handle handle;
	amdgpu_va_handle va_handle;
	uint64_t addr;
	uint64_t size;
	uint8_t *ptr;
};

struct amdgpu_vcn_reg {
	uint32_t data0;
	uint32_t data1;
	uint32_t cmd;
	uint32_t nop;
	uint32_t cntl;
};

static amdgpu_device_handle device_handle;
static uint32_t major_version;
static uint32_t minor_version;
static uint32_t family_id;
static uint32_t chip_rev;
static uint32_t chip_id;
static uint32_t asic_id;
static uint32_t chip_rev;
static uint32_t chip_id;

static amdgpu_context_handle context_handle;
static amdgpu_bo_handle ib_handle;
static amdgpu_va_handle ib_va_handle;
static uint64_t ib_mc_address;
static uint32_t *ib_cpu;

static amdgpu_bo_handle resources[MAX_RESOURCES];
static unsigned num_resources;
static struct amdgpu_vcn_reg reg;

static void amdgpu_cs_vcn_dec_create(void);
static void amdgpu_cs_vcn_dec_decode(void);
static void amdgpu_cs_vcn_dec_destroy(void);

static void amdgpu_cs_vcn_enc_create(void);
static void amdgpu_cs_vcn_enc_encode(void);
static void amdgpu_cs_vcn_enc_destroy(void);

static void amdgpu_cs_vcn_enc_shared(void);

CU_TestInfo vcn_tests[] = {

	{ "VCN DEC create",  amdgpu_cs_vcn_dec_create },
	{ "VCN DEC decode",  amdgpu_cs_vcn_dec_decode },
	{ "VCN DEC destroy",  amdgpu_cs_vcn_dec_destroy },

	{ "VCN ENC create",  amdgpu_cs_vcn_enc_create },
	{ "VCN ENC decode",  amdgpu_cs_vcn_enc_encode },
	{ "VCN ENC destroy",  amdgpu_cs_vcn_enc_destroy },

	{ "VCN ENC + GFX + compute shared buffer",	amdgpu_cs_vcn_enc_shared },
	CU_TEST_INFO_NULL,
};

CU_BOOL suite_vcn_tests_enable(void)
{
	struct drm_amdgpu_info_hw_ip info;
	int r;

	// if (amdgpu_device_initialize(drm_amdgpu[0], &major_version,
	// 			   &minor_version, &device_handle))
	// 	return CU_FALSE;

	// family_id = device_handle->info.family_id;
	// asic_id = device_handle->info.asic_id;
	// chip_rev = device_handle->info.chip_rev;
	// chip_id = device_handle->info.chip_external_rev;

	// r = amdgpu_query_hw_ip_info(device_handle, AMDGPU_HW_IP_VCN_DEC, 0, &info);

	// if (amdgpu_device_deinitialize(device_handle))
	// 		return CU_FALSE;

	// if (r != 0 || !info.available_rings ||
	//     (family_id < AMDGPU_FAMILY_RV &&
	//      (family_id == AMDGPU_FAMILY_AI &&
	//       (chip_id - chip_rev) < 0x32))) {  /* Arcturus */
	// 	printf("\n\nThe ASIC NOT support VCN, suite disabled\n");
	// 	return CU_FALSE;
	// }

	// if (family_id == AMDGPU_FAMILY_AI) {
	// 	amdgpu_set_test_active("VCN Tests", "VCN ENC create", CU_FALSE);
	// 	amdgpu_set_test_active("VCN Tests", "VCN ENC decode", CU_FALSE);
	// 	amdgpu_set_test_active("VCN Tests", "VCN ENC destroy", CU_FALSE);
	// }

	// if (family_id == AMDGPU_FAMILY_RV) {
	// 	if (chip_id >= (chip_rev + 0x91)) {
	// 		reg.data0 = 0x504;
	// 		reg.data1 = 0x505;
	// 		reg.cmd = 0x503;
	// 		reg.nop = 0x53f;
	// 		reg.cntl = 0x506;
	// 	} else {
			reg.data0 = 0x81c4;
			reg.data1 = 0x81c5;
			reg.cmd = 0x81c3;
			reg.nop = 0x81ff;
			reg.cntl = 0x81c6;
	// 	}
	// } else if (family_id == AMDGPU_FAMILY_NV) {
	// 	if (chip_id == (chip_rev + 0x28) ||
	// 	    chip_id == (chip_rev + 0x32) ||
	// 	    chip_id == (chip_rev + 0x3c)) {
	// 		reg.data0 = 0x10;
	// 		reg.data1 = 0x11;
	// 		reg.cmd = 0xf;
	// 		reg.nop = 0x29;
	// 		reg.cntl = 0x26d;
	// 	}
	// 	else {
	// 		reg.data0 = 0x504;
	// 		reg.data1 = 0x505;
	// 		reg.cmd = 0x503;
	// 		reg.nop = 0x53f;
	// 		reg.cntl = 0x506;
	// 	}
	// } else if (family_id == AMDGPU_FAMILY_AI) {
	// 	reg.data0 = 0x10;
	// 	reg.data1 = 0x11;
	// 	reg.cmd = 0xf;
	// 	reg.nop = 0x29;
	// 	reg.cntl = 0x26d;
	// } else
	// 	return CU_FALSE;

	return CU_TRUE;
}

int suite_vcn_tests_init(void)
{
	int r;

		return CUE_SUCCESS;


	r = amdgpu_device_initialize(drm_amdgpu[0], &major_version,
				     &minor_version, &device_handle);
	if (r)
		return CUE_SINIT_FAILED;

	family_id = device_handle->info.family_id;

	r = amdgpu_cs_ctx_create(device_handle, &context_handle);
	if (r)
		return CUE_SINIT_FAILED;

	r = amdgpu_bo_alloc_and_map(device_handle, IB_SIZE, 4096,
				    AMDGPU_GEM_DOMAIN_GTT, 0,
				    &ib_handle, (void**)&ib_cpu,
				    &ib_mc_address, &ib_va_handle);
	if (r)
		return CUE_SINIT_FAILED;

	return CUE_SUCCESS;
}

int suite_vcn_tests_clean(void)
{
	int r;

	r = amdgpu_bo_unmap_and_free(ib_handle, ib_va_handle,
			     ib_mc_address, IB_SIZE);
	if (r)
		return CUE_SCLEAN_FAILED;

	r = amdgpu_cs_ctx_free(context_handle);
	if (r)
		return CUE_SCLEAN_FAILED;

	r = amdgpu_device_deinitialize(device_handle);
	if (r)
		return CUE_SCLEAN_FAILED;

	return CUE_SUCCESS;
}

static int submit(unsigned ndw, unsigned ip)
{
	struct amdgpu_cs_request ibs_request = {0};
	struct amdgpu_cs_ib_info ib_info = {0};
	struct amdgpu_cs_fence fence_status = {0};
	uint32_t expired;
	int r;

	ib_info.ib_mc_address = ib_mc_address;
	ib_info.size = ndw;

	ibs_request.ip_type = ip;

	r = amdgpu_bo_list_create(device_handle, num_resources, resources,
				  NULL, &ibs_request.resources);
	if (r)
		return r;

	ibs_request.number_of_ibs = 1;
	ibs_request.ibs = &ib_info;
	ibs_request.fence_info.handle = NULL;

	r = amdgpu_cs_submit(context_handle, 0, &ibs_request, 1);
	if (r)
		return r;

	r = amdgpu_bo_list_destroy(ibs_request.resources);
	if (r)
		return r;

	fence_status.context = context_handle;
	fence_status.ip_type = ip;
	fence_status.fence = ibs_request.seq_no;

	r = amdgpu_cs_query_fence_status(&fence_status,
					 AMDGPU_TIMEOUT_INFINITE,
					 0, &expired);
	if (r)
		return r;

	return 0;
}

static void alloc_resource(struct amdgpu_vcn_bo *vcn_bo,
			unsigned size, unsigned domain)
{
	struct amdgpu_bo_alloc_request req = {0};
	amdgpu_bo_handle buf_handle;
	amdgpu_va_handle va_handle;
	uint64_t va = 0;
	int r;

	req.alloc_size = ALIGN(size, 4096);
	req.preferred_heap = domain;
	r = amdgpu_bo_alloc(device_handle, &req, &buf_handle);
	CU_ASSERT_EQUAL(r, 0);
	r = amdgpu_va_range_alloc(device_handle,
				  amdgpu_gpu_va_range_general,
				  req.alloc_size, 1, 0, &va,
				  &va_handle, 0);
	CU_ASSERT_EQUAL(r, 0);
	r = amdgpu_bo_va_op(buf_handle, 0, req.alloc_size, va, 0,
			    AMDGPU_VA_OP_MAP);
	CU_ASSERT_EQUAL(r, 0);
	vcn_bo->addr = va;
	vcn_bo->handle = buf_handle;
	vcn_bo->size = req.alloc_size;
	vcn_bo->va_handle = va_handle;
	r = amdgpu_bo_cpu_map(vcn_bo->handle, (void **)&vcn_bo->ptr);
	CU_ASSERT_EQUAL(r, 0);
	memset(vcn_bo->ptr, 0, size);
	r = amdgpu_bo_cpu_unmap(vcn_bo->handle);
	CU_ASSERT_EQUAL(r, 0);
}

static void free_resource(struct amdgpu_vcn_bo *vcn_bo)
{
	int r;

	r = amdgpu_bo_va_op(vcn_bo->handle, 0, vcn_bo->size,
			    vcn_bo->addr, 0, AMDGPU_VA_OP_UNMAP);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_va_range_free(vcn_bo->va_handle);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_bo_free(vcn_bo->handle);
	CU_ASSERT_EQUAL(r, 0);
	memset(vcn_bo, 0, sizeof(*vcn_bo));
}

static void vcn_dec_cmd(uint64_t addr, unsigned cmd, int *idx)
{
	ib_cpu[(*idx)++] = reg.data0;
	ib_cpu[(*idx)++] = addr;
	ib_cpu[(*idx)++] = reg.data1;
	ib_cpu[(*idx)++] = addr >> 32;
	ib_cpu[(*idx)++] = reg.cmd;
	ib_cpu[(*idx)++] = cmd << 1;
}

static void amdgpu_cs_vcn_dec_create(void)
{
	struct amdgpu_vcn_bo msg_buf;
	int len, r;

	num_resources  = 0;
	alloc_resource(&msg_buf, 4096, AMDGPU_GEM_DOMAIN_GTT);
	resources[num_resources++] = msg_buf.handle;
	resources[num_resources++] = ib_handle;

	r = amdgpu_bo_cpu_map(msg_buf.handle, (void **)&msg_buf.ptr);
	CU_ASSERT_EQUAL(r, 0);

	memset(msg_buf.ptr, 0, 4096);
	memcpy(msg_buf.ptr, vcn_dec_create_msg, sizeof(vcn_dec_create_msg));

	len = 0;
	ib_cpu[len++] = reg.data0;
	ib_cpu[len++] = msg_buf.addr;
	ib_cpu[len++] = reg.data1;
	ib_cpu[len++] = msg_buf.addr >> 32;
	ib_cpu[len++] = reg.cmd;
	ib_cpu[len++] = 0;
	for (; len % 16; ) {
		ib_cpu[len++] = reg.nop;
		ib_cpu[len++] = 0;
	}

	r = submit(len, AMDGPU_HW_IP_VCN_DEC);
	CU_ASSERT_EQUAL(r, 0);

	free_resource(&msg_buf);
}

static void amdgpu_cs_vcn_dec_decode(void)
{
	const unsigned dpb_size = 15923584, dt_size = 737280;
	uint64_t msg_addr, fb_addr, bs_addr, dpb_addr, ctx_addr, dt_addr, it_addr, sum;
	struct amdgpu_vcn_bo dec_buf;
	int size, len, i, r;
	uint8_t *dec;

	size = 4*1024; /* msg */
	size += 4*1024; /* fb */
	size += 4096; /*it_scaling_table*/
	size += ALIGN(sizeof(uvd_bitstream), 4*1024);
	size += ALIGN(dpb_size, 4*1024);
	size += ALIGN(dt_size, 4*1024);

	num_resources  = 0;
	alloc_resource(&dec_buf, size, AMDGPU_GEM_DOMAIN_GTT);
	resources[num_resources++] = dec_buf.handle;
	resources[num_resources++] = ib_handle;

	r = amdgpu_bo_cpu_map(dec_buf.handle, (void **)&dec_buf.ptr);
	dec = dec_buf.ptr;

	CU_ASSERT_EQUAL(r, 0);
	memset(dec_buf.ptr, 0, size);
	memcpy(dec_buf.ptr, vcn_dec_decode_msg, sizeof(vcn_dec_decode_msg));
	memcpy(dec_buf.ptr + sizeof(vcn_dec_decode_msg),
			avc_decode_msg, sizeof(avc_decode_msg));

	dec += 4*1024;
	memcpy(dec, feedback_msg, sizeof(feedback_msg));
	dec += 4*1024;
	memcpy(dec, uvd_it_scaling_table, sizeof(uvd_it_scaling_table));

	dec += 4*1024;
	memcpy(dec, uvd_bitstream, sizeof(uvd_bitstream));

	dec += ALIGN(sizeof(uvd_bitstream), 4*1024);

	dec += ALIGN(dpb_size, 4*1024);

	msg_addr = dec_buf.addr;
	fb_addr = msg_addr + 4*1024;
	it_addr = fb_addr + 4*1024;
	bs_addr = it_addr + 4*1024;
	dpb_addr = ALIGN(bs_addr + sizeof(uvd_bitstream), 4*1024);
	ctx_addr = ALIGN(dpb_addr + 0x006B9400, 4*1024);
	dt_addr = ALIGN(dpb_addr + dpb_size, 4*1024);

	len = 0;
	vcn_dec_cmd(msg_addr, 0x0, &len);
	vcn_dec_cmd(dpb_addr, 0x1, &len);
	vcn_dec_cmd(dt_addr, 0x2, &len);
	vcn_dec_cmd(fb_addr, 0x3, &len);
	vcn_dec_cmd(bs_addr, 0x100, &len);
	vcn_dec_cmd(it_addr, 0x204, &len);
	vcn_dec_cmd(ctx_addr, 0x206, &len);

	ib_cpu[len++] = reg.cntl;
	ib_cpu[len++] = 0x1;
	for (; len % 16; ) {
		ib_cpu[len++] = reg.nop;
		ib_cpu[len++] = 0;
	}

	r = submit(len, AMDGPU_HW_IP_VCN_DEC);
	CU_ASSERT_EQUAL(r, 0);

	for (i = 0, sum = 0; i < dt_size; ++i)
		sum += dec[i];

	CU_ASSERT_EQUAL(sum, SUM_DECODE);

	free_resource(&dec_buf);
}

static void amdgpu_cs_vcn_dec_destroy(void)
{
	struct amdgpu_vcn_bo msg_buf;
	int len, r;

	num_resources  = 0;
	alloc_resource(&msg_buf, 1024, AMDGPU_GEM_DOMAIN_GTT);
	resources[num_resources++] = msg_buf.handle;
	resources[num_resources++] = ib_handle;

	r = amdgpu_bo_cpu_map(msg_buf.handle, (void **)&msg_buf.ptr);
	CU_ASSERT_EQUAL(r, 0);

	memset(msg_buf.ptr, 0, 1024);
	memcpy(msg_buf.ptr, vcn_dec_destroy_msg, sizeof(vcn_dec_destroy_msg));

	len = 0;
	ib_cpu[len++] = reg.data0;
	ib_cpu[len++] = msg_buf.addr;
	ib_cpu[len++] = reg.data1;
	ib_cpu[len++] = msg_buf.addr >> 32;
	ib_cpu[len++] = reg.cmd;
	ib_cpu[len++] = 0;
	for (; len % 16; ) {
		ib_cpu[len++] = reg.nop;
		ib_cpu[len++] = 0;
	}

	r = submit(len, AMDGPU_HW_IP_VCN_DEC);
	CU_ASSERT_EQUAL(r, 0);

	free_resource(&msg_buf);
}

static void amdgpu_cs_vcn_enc_create(void)
{
	/* TODO */
}

static void amdgpu_cs_vcn_enc_encode(void)
{
	/* TODO */
}

static void amdgpu_cs_vcn_enc_destroy(void)
{
	/* TODO */
}

#define GFX_COMPUTE_NOP  0xffff1000
#define GFX_COMPUTE_NOP_SI 0x80000000

#define	PACKET_TYPE3	3

#define PACKET3(op, n)	((PACKET_TYPE3 << 30) |				\
			 (((op) & 0xFF) << 8) |				\
			 ((n) & 0x3FFF) << 16)
#define PACKET3_COMPUTE(op, n) PACKET3(op, n) | (1 << 1)

/* Packet 3 types */
#define	PACKET3_NOP					0x10
#define PACKET3_COPY_DATA                         0x40
#define		COUNT_SEL                             (1 << 16)

#define	PACKET3_WRITE_DATA				0x37
#define		WRITE_DATA_DST_SEL(x)                   ((x) << 8)
		/* 0 - register
		 * 1 - memory (sync - via GRBM)
		 * 2 - gl2
		 * 3 - gds
		 * 4 - reserved
		 * 5 - memory (async - direct)
		 */
#define		WR_ONE_ADDR                             (1 << 16)
#define		WR_CONFIRM                              (1 << 20)
#define		WRITE_DATA_CACHE_POLICY(x)              ((x) << 25)
		/* 0 - LRU
		 * 1 - Stream
		 */
#define		WRITE_DATA_ENGINE_SEL(x)                ((x) << 30)
		/* 0 - me
		 * 1 - pfp
		 * 2 - ce
		 */


#define SWAP_32(num) (((num & 0xff000000) >> 24) | \
		      ((num & 0x0000ff00) << 8) | \
		      ((num & 0x00ff0000) >> 8) | \
		      ((num & 0x000000ff) << 24))


static  uint32_t shader_bin[] = {
	SWAP_32(0x800082be), SWAP_32(0x02ff08bf), SWAP_32(0x7f969800), SWAP_32(0x040085bf),
	SWAP_32(0x02810281), SWAP_32(0x02ff08bf), SWAP_32(0x7f969800), SWAP_32(0xfcff84bf),
	SWAP_32(0xff0083be), SWAP_32(0x00f00000), SWAP_32(0xc10082be), SWAP_32(0xaa02007e),
	SWAP_32(0x000070e0), SWAP_32(0x00000080), SWAP_32(0x000081bf)
};

#define CODE_OFFSET 512
#define DATA_OFFSET 1024

#define PKT3_SET_SH_REG                        0x76
#define		PACKET3_SET_SH_REG_START			0x00002c00

#define	PACKET3_DISPATCH_DIRECT				0x15
#define mmCOMPUTE_PGM_LO                                                        0x2e0c
#define mmCOMPUTE_PGM_RSRC1                                                     0x2e12
#define mmCOMPUTE_TMPRING_SIZE                                                  0x2e18
#define mmCOMPUTE_USER_DATA_0                                                   0x2e40
#define mmCOMPUTE_USER_DATA_1                                                   0x2e41
#define mmCOMPUTE_RESOURCE_LIMITS                                               0x2e15
#define mmCOMPUTE_NUM_THREAD_X                                                  0x2e07

#define PKT3_CONTEXT_CONTROL                   0x28
#define     CONTEXT_CONTROL_LOAD_ENABLE(x)     (((unsigned)(x) & 0x1) << 31)
#define     CONTEXT_CONTROL_LOAD_CE_RAM(x)     (((unsigned)(x) & 0x1) << 28)
#define     CONTEXT_CONTROL_SHADOW_ENABLE(x)   (((unsigned)(x) & 0x1) << 31)

#define PKT3_CLEAR_STATE                       0x12


#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
typedef struct
{
  int id;
  size_t size;
} shm_t;

shm_t *shm_new(size_t size)
{
  shm_t *shm = calloc(1, sizeof *shm);
  shm->size = size;

  if ((shm->id = shmget(IPC_PRIVATE, size, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR)) < 0)
  {
    perror("shmget");
    free(shm);
    return NULL;
  }

  return shm;
}

void shm_write(shm_t *shm, void *data)
{
  void *shm_data;

  if ((shm_data = shmat(shm->id, NULL, 0)) == (void *) -1)
  {
    perror("write");
    return;
  }

  memcpy(shm_data, data, shm->size);
  shmdt(shm_data);
}

void shm_read(void *data, shm_t *shm)
{
  void *shm_data;

  if ((shm_data = shmat(shm->id, NULL, 0)) == (void *) -1)
  {
    perror("read");
    return;
  }
  memcpy(data, shm_data, shm->size);
  shmdt(shm_data);
}

void shm_del(shm_t *shm)
{
  shmctl(shm->id, IPC_RMID, 0);
  free(shm);
}

static void amdgpu_cs_vcn_enc_shared(void)
{
	amdgpu_bo_handle ib_gfx_handle;
	void *ib_gfx_cpu;
	uint64_t ib_gfx_mc_address;

	amdgpu_bo_handle ib_compute_handle;
	void *ib_compute_cpu;
	uint64_t ib_compute_mc_address;

	struct amdgpu_cs_request ibs_request;
	struct amdgpu_cs_ib_info ib_info;
	amdgpu_bo_list_handle bo_list;
	amdgpu_va_handle va_handle[3];
	struct amdgpu_cs_fence fence_status = {0};
	struct drm_amdgpu_info_hw_ip info;

	int i, j, r;
	int write_length = 128;
	uint32_t *ptr, sync_obj_handle, sync_obj_handle2;

	amdgpu_context_handle context_handle;

	amdgpu_bo_handle bo;
	uint64_t bo_mc;
	volatile uint32_t *bo_cpu;

	uint64_t shader_dst;

	amdgpu_bo_handle resources[3];

	uint64_t imported_bo_va = 0;

	uint32_t *pm4;
	const int pm4_dw = 256;

	uint32_t expired;
	int pid;

	// undo the stuff previous setup
	// r = amdgpu_cs_ctx_free(context_handle);
	// r = amdgpu_device_deinitialize(device_handle);

	pid = fork();

	// pid == 0 - compute
	// pid != 0 - gfx

	r = amdgpu_device_initialize(drm_amdgpu[0], &major_version,
					&minor_version, &device_handle);

	printf("PID: %d, device handle: %d\n", pid, device_handle);

	// device_handle_gfx = device_handle_compute = device_handle;

	// r = amdgpu_query_hw_ip_info(device_handle, AMDGPU_HW_IP_COMPUTE, 0, &info);
	// CU_ASSERT_EQUAL(r, 0);
	// info.available_rings;

	// create context
	r = amdgpu_cs_ctx_create(device_handle, &context_handle);
	CU_ASSERT_EQUAL(r, 0);


	memset(&ib_info, 0, sizeof(struct amdgpu_cs_ib_info));

	// IB memory
	r = amdgpu_bo_alloc_and_map(device_handle, 8192, 4096,
		AMDGPU_GEM_DOMAIN_GTT, 0,
		&ib_handle, &ib_cpu,
		&ib_mc_address, &va_handle[0]);
	CU_ASSERT_EQUAL(r, 0);

	// test buffer
	r = amdgpu_bo_alloc_and_map(device_handle, 4096, 4096,
		AMDGPU_GEM_DOMAIN_GTT, 0,
		&bo, &bo_cpu,
		&bo_mc, &va_handle[1]);
	CU_ASSERT_EQUAL(r, 0);

	uint32_t shared_handle = 0;
	struct amdgpu_bo_import_result res = {0};

	shm_t *shm = shm_new(sizeof shared_handle);

	if (pid) {
		// gpu - export
		r = amdgpu_bo_export(bo, amdgpu_bo_handle_type_dma_buf_fd, &shared_handle);
		shm_write(shm, &shared_handle);
		printf("exported %x", shared_handle);
		CU_ASSERT_EQUAL(r, 0);
	}
	else {
		// compute - import
		while(!shared_handle)
			shm_read(&shared_handle, shm);
		printf("importing %x", shared_handle);
		r = amdgpu_bo_import(device_handle, amdgpu_bo_handle_type_dma_buf_fd, shared_handle, &res);
		CU_ASSERT_EQUAL(r, 0);
		r = amdgpu_va_range_alloc(device_handle,
					amdgpu_gpu_va_range_general,
					4096, 1, 0, &imported_bo_va,
					&va_handle[2], 0);
		CU_ASSERT_EQUAL(r, 0);
		r = amdgpu_bo_va_op(res.buf_handle, 0, 4096, imported_bo_va, 0, AMDGPU_VA_OP_MAP);
		CU_ASSERT_EQUAL(r, 0);
	}


	if (pid) {
		// cpu - fill memory with value A
		ptr = bo_cpu;
		for (j = 0; j < write_length; ++j)
			ptr[j] = 0xdeaddead;
	}

	// copy shader code to video memory (IB + offset)
	ptr = ib_cpu;
	memcpy(ptr + CODE_OFFSET , shader_bin, sizeof(shader_bin));

	j = i = 0;
	pm4 = ib_cpu;

	// ib_info[0].ib_mc_address = ib_compute_mc_address;
	// ib_info[0].size = i;

	// j = i;

	// long shader task

	if (pid) {
		/* Dispatch minimal init config and verify it's executed */
		pm4[i++] = PACKET3(PKT3_CONTEXT_CONTROL, 1);
		pm4[i++] = 0x80000000;
		pm4[i++] = 0x80000000;

		pm4[i++] = PACKET3(PKT3_CLEAR_STATE, 0);
		pm4[i++] = 0x80000000;
	}

	/* Program compute regs */
	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_PGM_LO - PACKET3_SET_SH_REG_START;
	pm4[i++] = (ib_mc_address + CODE_OFFSET * 4) >> 8;
	pm4[i++] = (ib_mc_address + CODE_OFFSET * 4) >> 40;


	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_PGM_RSRC1 - PACKET3_SET_SH_REG_START;
	/*
	 * 002c0040         COMPUTE_PGM_RSRC1 <- VGPRS = 0
	                                      SGPRS = 1
	                                      PRIORITY = 0
	                                      FLOAT_MODE = 192 (0xc0)
	                                      PRIV = 0
	                                      DX10_CLAMP = 1
	                                      DEBUG_MODE = 0
	                                      IEEE_MODE = 0
	                                      BULKY = 0
	                                      CDBG_USER = 0
	 *
	 */
	pm4[i++] = 0x002c0040;


	/*
	 * 00000010         COMPUTE_PGM_RSRC2 <- SCRATCH_EN = 0
	                                      USER_SGPR = 8
	                                      TRAP_PRESENT = 0
	                                      TGID_X_EN = 0
	                                      TGID_Y_EN = 0
	                                      TGID_Z_EN = 0
	                                      TG_SIZE_EN = 0
	                                      TIDIG_COMP_CNT = 0
	                                      EXCP_EN_MSB = 0
	                                      LDS_SIZE = 0
	                                      EXCP_EN = 0
	 *
	 */
	pm4[i++] = 0x00000010;


/*
 * 00000100         COMPUTE_TMPRING_SIZE <- WAVES = 256 (0x100)
                                         WAVESIZE = 0
 *
 */
	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 1);
	pm4[i++] = mmCOMPUTE_TMPRING_SIZE - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0x00000100;

	if (pid) {	/* gfx */
		shader_dst = bo_mc;
	}
	else {	    /* compute */
		shader_dst = ib_mc_address + DATA_OFFSET * 4;
	}

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_USER_DATA_0 - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0xffffffff & shader_dst;
	pm4[i++] = (0xffffffff00000000 & shader_dst) >> 32;

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 1);
	pm4[i++] = mmCOMPUTE_RESOURCE_LIMITS - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0;

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 3);
	pm4[i++] = mmCOMPUTE_NUM_THREAD_X - PACKET3_SET_SH_REG_START;
	pm4[i++] = 1;
	pm4[i++] = 1;
	pm4[i++] = 1;


	/* Dispatch */
	pm4[i++] = PACKET3(PACKET3_DISPATCH_DIRECT, 3);
	pm4[i++] = 1;
	pm4[i++] = 1;
	pm4[i++] = 1;
	pm4[i++] = 0x00000045; /* DISPATCH DIRECT field */

	while (i & 7)
		pm4[i++] =  0xffff1000; /* type3 nop packet */

	if (!pid) {
		// compute - copy memory to new memory
		pm4[i++] = PACKET3_COMPUTE(PACKET3_COPY_DATA, 4);
		pm4[i++] = WRITE_DATA_DST_SEL(5) | COUNT_SEL | WR_CONFIRM | 1;
		pm4[i++] = 0xfffffffc & imported_bo_va;
		pm4[i++] = (0xffffffff00000000 & imported_bo_va) >> 32;
		pm4[i++] = 0xfffffffc & bo_mc;
		pm4[i++] = (0xffffffff00000000 & bo_mc) >> 32;

		while (i & 7)
			pm4[i++] = 0xffff1000; //PACKET3(PACKET3_NOP, 14); //GFX_COMPUTE_NOP_SI; /* type3 nop packet */
	}

	ib_info.ib_mc_address = ib_mc_address; // + j * 4;
	ib_info.size = i; // - j;


	// submit IBs
	if (pid) {
		resources[0] = ib_gfx_handle;
		resources[1] = bo;
		r = amdgpu_bo_list_create(device_handle, 2, resources, NULL, &bo_list);
	}
	else {
		resources[0] = ib_compute_handle;
		resources[1] = bo;
		resources[2] = res.buf_handle;
		r = amdgpu_bo_list_create(device_handle, 3, resources, NULL, &bo_list);
	}

	int seq_no;

	memset(&ibs_request, 0, sizeof(struct amdgpu_cs_request));

	if (pid) {
		ibs_request.ip_type = AMDGPU_HW_IP_GFX;
		ibs_request.ring = 0;
		ibs_request.number_of_ibs = 1;
		ibs_request.ibs = &ib_info;
		ibs_request.resources = bo_list;
		CU_ASSERT_EQUAL(amdgpu_cs_submit(context_handle, 0, &ibs_request, 1), 0);

		seq_no = ibs_request.seq_no;

		fence_status.context = context_handle;
		fence_status.ip_type = AMDGPU_HW_IP_GFX;
		fence_status.ip_instance = 0;
		fence_status.ring = 0;
		fence_status.fence = seq_no;
	}
	else {
		ibs_request.ip_type = AMDGPU_HW_IP_COMPUTE;
		ibs_request.ring = 0;
		ibs_request.number_of_ibs = 1;
		ibs_request.ibs = &ib_info;
		ibs_request.resources = bo_list;
		ibs_request.fence_info.handle = NULL;
		CU_ASSERT_EQUAL(amdgpu_cs_submit(context_handle, 0, &ibs_request, 1), 0);

		seq_no = ibs_request.seq_no;

		fence_status.context = context_handle;
		fence_status.ip_type = AMDGPU_HW_IP_COMPUTE;
		fence_status.ip_instance = 0;
		fence_status.ring = 0;
		fence_status.fence = seq_no;
	}

	r = amdgpu_cs_query_fence_status(&fence_status,
					 AMDGPU_TIMEOUT_INFINITE,
					 0, &expired);


	if (!pid) {
		// compute - new memory should contain value B
		i = 0;
		while (++i < 2) {
			CU_ASSERT_EQUAL(((int*)bo_cpu)[i], 0xbeefbeef);
		}
	}

	// clean up
	if (!pid) {
		r = amdgpu_bo_va_op(res.buf_handle, 0, 4096, imported_bo_va, 0, AMDGPU_VA_OP_UNMAP);
		CU_ASSERT_EQUAL(r, 0);

		r = amdgpu_va_range_free(va_handle[2]);
		CU_ASSERT_EQUAL(r, 0);

		r = amdgpu_bo_free(res.buf_handle);
		CU_ASSERT_EQUAL(r, 0);
	}

	r = amdgpu_bo_unmap_and_free(ib_handle, va_handle[0],
		ib_mc_address, IB_SIZE);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_bo_unmap_and_free(bo, va_handle[1],
		bo_mc, IB_SIZE);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_cs_ctx_free(context_handle);
	CU_ASSERT_EQUAL(r, 0);
}